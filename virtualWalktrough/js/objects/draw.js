const drawFigures = () => {
    fetch(`${HOST}/object`)
        .then(response => response.json())
        .then(data => {
            data.map(obj => {
                new THREE.MTLLoader()
                .setPath('resources/OBJ/')
                .load(obj.path + '.mtl', function (materials) {
                    materials.preload()
                    new THREE.OBJLoader()
                        .setMaterials(materials)
                        .setPath('resources/OBJ/')
                        .load(obj.path + '.obj', function (object) {
                            object.position.y = obj.coors.y
                            object.position.x = obj.coors.x
                            object.position.z = obj.coors.z
                            scene.add(object)
                        }, onProgress, onError)
                })
            })
        })
}

var onProgress = function (xhr) {
    if (xhr.lengthComputable) {
        var percentComplete = xhr.loaded / xhr.total * 100;
        console.log(Math.round(percentComplete, 2) + '% downloaded');
    }
}
var onError = function () { };